﻿namespace FlightBooking.Core.Classes.Rules
{
    public abstract class RuleBase
    {
        public abstract double FractionOfPricePaid { get; }
        public abstract int BaggageAllowed { get; }
    }
}