﻿using FlightBooking.Core.Classes.Rules;
using FlightBooking.Core.Enums;

namespace FlightBooking.Core.Classes
{
    public class Passenger
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int AllowedBags => Rules.BaggageAllowed;

        private int _loyaltyPoints;
        //discounted passengers cannot accrue loyalty points
        public int LoyaltyPoints
        {
            get { return Type == PassengerType.Discounted ? 0 : _loyaltyPoints; }
            set { _loyaltyPoints = Type == PassengerType.Discounted ? 0 : value;  }
        }

        public bool IsUsingLoyaltyPoints { get; set; }

        public PassengerType Type { get; set; }

        public RuleBase Rules
        {
            get
            {
                switch (Type)
                {
                    case PassengerType.General:
                        return new GeneralPassengerRules();
                    case PassengerType.AirlineEmployee:
                        return new AirlineEmployeePassengerRules();
                    case PassengerType.Discounted:
                        return new DiscountedPassengerRules();
                    case PassengerType.LoyaltyMember:
                        return new LoyaltyPassengerRules();
                }
                return new GeneralPassengerRules();
            }
        }

        public Passenger() { }

        public Passenger(PassengerType type, string[] passengerSegments)
        {
            Type = type;
            Name = passengerSegments[2];
            Age = int.Parse(passengerSegments[3]);

            if (type != PassengerType.LoyaltyMember)
                return;

            LoyaltyPoints = int.Parse(passengerSegments[4]);
            IsUsingLoyaltyPoints = bool.Parse(passengerSegments[5]);
        }
    }
}