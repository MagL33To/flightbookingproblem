﻿namespace FlightBooking.Core.Enums
{
    public enum PassengerType
    {
        General,
        LoyaltyMember,
        AirlineEmployee,
        Discounted
    }
}