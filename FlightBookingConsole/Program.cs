﻿using System;
using System.Collections.Generic;
using FlightBooking.Core.Classes;
using FlightBooking.Core.Enums;

namespace FlightBookingProblem
{
    public class Program
    {
        private static readonly List<FlightRoute> Routes = new List<FlightRoute> {
            new FlightRoute
            {
                Origin = "London",
                Destination = "Paris",
                BaseCost = 50,
                BasePrice = 100,
                LoyaltyPointsGained = 5,
                MinimumTakeOffPercentage = 0.7
            }};
        private static readonly List<Plane> Planes = new List<Plane>
        {
            new Plane { Id = 123, Name = "Antonov AN-2", NumberOfSeats = 12 },
            new Plane { Id = 124, Name = "Bombardier Q400", NumberOfSeats = 78 },
            new Plane { Id = 125, Name = "ATR 640", NumberOfSeats = 74 }
        };
        
        static void Main(string[] args)
        {
            var scheduledFlight = new ScheduledFlight(Routes[0], Planes[0]);
            
            string command;
            do
            {
                command = Console.ReadLine() ?? "";
                var enteredText = command.ToLower();
                if (enteredText.Contains("add general"))
                    scheduledFlight.AddPassenger(new Passenger(PassengerType.General, enteredText.Split(' ')));
                else if (enteredText.Contains("add loyalty"))
                    scheduledFlight.AddPassenger(new Passenger(PassengerType.LoyaltyMember, enteredText.Split(' ')));
                else if (enteredText.Contains("add airline"))
                    scheduledFlight.AddPassenger(new Passenger(PassengerType.AirlineEmployee, enteredText.Split(' ')));
                else if (enteredText.Contains("add discounted"))
                    scheduledFlight.AddPassenger(new Passenger(PassengerType.Discounted, enteredText.Split(' ')));
                else if (enteredText.Contains("print summary"))
                {
                    var summarySegments = enteredText.Split(' ');

                    var type = summarySegments.Length == 2
                        ? RuleType.Normal
                        : (RuleType) Enum.Parse(typeof (RuleType), enteredText.Split(' ')[2]);

                    Console.WriteLine();
                    Console.WriteLine(scheduledFlight.GetSummary(type, Planes));
                }
                else
                {
                    //switch is faster but can only be used with compile-time constants
                    switch (enteredText)
                    {
                        case "exit":
                            Environment.Exit(1);
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("UNKNOWN INPUT");
                            Console.ResetColor();
                            break;
                    }
                }
            } while (command != "exit");
        }
    }
}