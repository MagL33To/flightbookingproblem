﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightBooking.Core.Enums;

namespace FlightBooking.Core.Classes
{
    public class ScheduledFlight
    {
        public ScheduledFlight(FlightRoute route, Plane plane)
        {
            Route = route;
            Aircraft = plane;
            Passengers = new List<Passenger>();
        }

        public FlightRoute Route { get; }
        public Plane Aircraft { get; }
        public IList<Passenger> Passengers { get; }
        public double CostOfFlight { get; private set; }
        public double ProfitFromFlight { get; private set; }
        public int TotalLoyaltyPointsAccrued { get; private set; }
        public int TotalLoyaltyPointsRedeemed { get; private set; }
        public int TotalExpectedBaggage { get; private set; }
        public int SeatsTaken { get; private set; }
        private double ProfitSurplus => ProfitFromFlight - CostOfFlight;
        
        private readonly string _newLine = Environment.NewLine;

        public void AddPassenger(Passenger passenger)
        {
            //depending on whether loyalty passenger is using loyalty points or not, they pay either
            //nothing or full price, so read the value into a variable as it may change.
            var toBePaid = passenger.Rules.FractionOfPricePaid;

            if (passenger.Type == PassengerType.LoyaltyMember)
            {
                if (passenger.IsUsingLoyaltyPoints)
                {
                    var loyaltyPointsRedeemed = Convert.ToInt32(Math.Ceiling(Route.BasePrice));
                    passenger.LoyaltyPoints -= loyaltyPointsRedeemed;
                    TotalLoyaltyPointsRedeemed += loyaltyPointsRedeemed;
                    toBePaid = 0;
                }
                else
                    TotalLoyaltyPointsAccrued += Route.LoyaltyPointsGained;
            }
            ProfitFromFlight += (Route.BasePrice*toBePaid);
            TotalExpectedBaggage += passenger.Rules.BaggageAllowed;
            CostOfFlight += Route.BaseCost;
            SeatsTaken++;
            Passengers.Add(passenger);
        }
        
        public string GetSummary(RuleType rules, IEnumerable<Plane> availablePlanes)
        {
            //First determine whether the flight can go ahead or not.
            var flightMayProceed = false;
            switch (rules)
            {
                case RuleType.Relaxed:
                    //fewer seats are taken than are on plane
                    if (SeatsTaken <= Aircraft.NumberOfSeats &&
                        //flight makes a profit or number of airline employees on board is greater than the take off percentage
                        (ProfitSurplus > 0 || Passengers.Count(x => x.Type == PassengerType.AirlineEmployee) / (double)Aircraft.NumberOfSeats > Route.MinimumTakeOffPercentage) &&
                        //there are more passengers on board than the take off percentage
                        SeatsTaken / (double)Aircraft.NumberOfSeats > Route.MinimumTakeOffPercentage)
                        flightMayProceed = true;
                    break;
                case RuleType.Normal:
                    //fewer seats are taken than are on plane
                    if (SeatsTaken <= Aircraft.NumberOfSeats &&
                        //flight makes a profit
                        ProfitSurplus > 0 &&
                        //there are more passengers on board than the take off percentage
                        SeatsTaken / (double)Aircraft.NumberOfSeats > Route.MinimumTakeOffPercentage)
                        flightMayProceed = true;
                    break;
            }

            var verticalWhiteSpace = Environment.NewLine + Environment.NewLine;
            const string indentation = "    ";

            //construct the summary string
            var result = new StringBuilder();

            result.Append($"Flight summary for {Route.Title}{verticalWhiteSpace}");

            result.Append($"Total passengers: {SeatsTaken}{_newLine}");
            result.Append($"{indentation}General sales: {Passengers.Count(p => p.Type == PassengerType.General)}{_newLine}");
            result.Append($"{indentation}Loyalty member sales: {Passengers.Count(p => p.Type == PassengerType.LoyaltyMember)}{_newLine}");
            result.Append($"{indentation}Airline employee comps: {Passengers.Count(p => p.Type == PassengerType.AirlineEmployee)}{_newLine}");
            result.Append($"{indentation}Discounted sales: {Passengers.Count(p => p.Type == PassengerType.Discounted)}{verticalWhiteSpace}");

            result.Append($"Total expected baggage: {TotalExpectedBaggage}{verticalWhiteSpace}");

            result.Append($"Total revenue from flight: {ProfitFromFlight}{_newLine}");
            result.Append($"Total costs from flight: {CostOfFlight}{_newLine}");

            result.Append($"{(ProfitSurplus > 0 ? "Flight generating profit of: " : "Flight making loss of: ")}{ProfitSurplus}{verticalWhiteSpace}");

            result.Append($"Total loyalty points given away: {TotalLoyaltyPointsAccrued}{_newLine}");
            result.Append($"Total loyalty points redeemed: {TotalLoyaltyPointsRedeemed}{_newLine}{verticalWhiteSpace}");

            result.Append(flightMayProceed ? "THIS FLIGHT MAY PROCEED." : "THIS FLIGHT MAY NOT PROCEED.");

            //if the flight is overbooked, check whether there are other suitable planes for this flight
            if (SeatsTaken > Aircraft.NumberOfSeats)
                result.Append(FindSuitableAircraft(availablePlanes));

            return result.ToString();
        }

        private string FindSuitableAircraft(IEnumerable<Plane> availablePlanes)
        {
            var suitableAircraft = availablePlanes.Where(x => x.NumberOfSeats > SeatsTaken).ToList();

            if (!suitableAircraft.Any())
                return String.Empty;

            var builder = new StringBuilder();
            foreach (var plane in suitableAircraft)
            {
                builder.Append($"{plane.Name} could handle this flight.{_newLine}");
            }

            //add a new line at the beginning of the replacements. Remove the extra one at the end.
            return _newLine + builder.ToString().Substring(0, builder.Length - _newLine.Length);
        }
    }
}