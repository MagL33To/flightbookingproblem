﻿namespace FlightBooking.Core.Classes.Rules
{
    public class AirlineEmployeePassengerRules : RuleBase
    {
        //airline employees travel for free
        public override double FractionOfPricePaid => 0;
        //airline employees are allowed one bag
        public override int BaggageAllowed => 1;
    }
}