﻿namespace FlightBooking.Core.Classes.Rules
{
    public class LoyaltyPassengerRules : RuleBase
    {
        //loyalty passenger pays full price unless they're using loyalty points to pay.
        //their price is removed if applicable when added to a flight
        public override double FractionOfPricePaid => 1;
        //loyalty passenger is allowed 2 bags
        public override int BaggageAllowed => 2;
    }
}