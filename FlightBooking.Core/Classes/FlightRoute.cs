﻿namespace FlightBooking.Core.Classes
{
    public class FlightRoute
    {
        //I've kept this constructor for the sake of compatability with other systems,
        //but I don't see what was gained by having origin and destination be private.
        public FlightRoute(string origin, string destination)
        {
            Origin = origin;
            Destination = destination;
        }

        public FlightRoute() { }

        public string Title { get { return Origin + " to " + Destination; } }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public double BasePrice { get; set; }
        public double BaseCost { get; set; }
        public int LoyaltyPointsGained { get; set; }
        public double MinimumTakeOffPercentage { get; set; }        
    }
}