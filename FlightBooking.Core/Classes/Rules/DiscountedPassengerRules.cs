﻿namespace FlightBooking.Core.Classes.Rules
{
    public class DiscountedPassengerRules : RuleBase
    {
        //discounted passengers pay half price
        public override double FractionOfPricePaid => 0.5;
        //discounted passengers cannot have bags
        public override int BaggageAllowed => 0;
    }
}