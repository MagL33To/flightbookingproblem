﻿using FlightBooking.Core.Classes;
using NUnit.Framework;

namespace FlightBooking.Test
{
    [TestFixture]
    public class FlightRouteTests
    {
        [Test]
        public void Constructor_OriginAndDestinationSupplied_CorrectFlightRouteReturned()
        {
            const string origin = "New York";
            const string destination = "San Francisco";

            var fr = new FlightRoute(origin, destination);

            Assert.AreEqual("New York to San Francisco", fr.Title);
        }
    }
}