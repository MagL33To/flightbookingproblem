﻿namespace FlightBooking.Core.Enums
{
    public enum RuleType
    {
        Normal = 1,
        Relaxed = 2
    }
}