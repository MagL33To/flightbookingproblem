#FlightBookingProblem

Please note that I have implemented the different flight requirements by adding a modifier to the "print summary" command. To use the original requirements, simply enter "print summary" as usual, or "print summary 1". To use the relaxed requirements, enter: "print summary 2".