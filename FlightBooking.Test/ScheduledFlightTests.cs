﻿using System.Collections.Generic;
using FlightBooking.Core.Classes;
using FlightBooking.Core.Enums;
using NUnit.Framework;

namespace FlightBooking.Test
{
    [TestFixture]
    public class ScheduledFlightTests
    {
        #region Test Objects
        private readonly List<Passenger> _testPassengers = new List<Passenger>
                 {
                     new Passenger
                     {
                         Name = "Test1",
                         Type = PassengerType.General,
                         Age = 20
                     },
                     new Passenger
                     {
                         Name = "Test2",
                         Type = PassengerType.LoyaltyMember,
                         Age = 21,
                         LoyaltyPoints = 1000,
                         IsUsingLoyaltyPoints = true
                     },
                     new Passenger
                     {
                         Name = "Test3",
                         Type = PassengerType.LoyaltyMember,
                         Age = 22,
                         LoyaltyPoints = 1500,
                         IsUsingLoyaltyPoints = false
                     },
                     new Passenger
                     {
                         Name = "Test4",
                         Type = PassengerType.AirlineEmployee,
                         Age = 23
                     },
                     new Passenger
                     {
                         Name = "Test5",
                         Type = PassengerType.Discounted,
                         Age = 24
                     },
                     new Passenger
                     {
                         Name = "Test6",
                         Type = PassengerType.Discounted,
                         Age = 26
                     },
                     new Passenger
                     {
                         Name = "Test7",
                         Type = PassengerType.General,
                         Age = 27
                     },
                     new Passenger
                     {
                         Name = "Test8",
                         Type = PassengerType.General,
                         Age = 28
                     },
                     new Passenger
                     {
                         Name = "Test9",
                         Type = PassengerType.General,
                         Age = 29
                     }
                 };

        private readonly List<Passenger> _employeePassengers = new List<Passenger>
                 {
                     new Passenger
                     {
                         Name = "Test1",
                         Type = PassengerType.AirlineEmployee,
                         Age = 20
                     },
                     new Passenger
                     {
                         Name = "Test2",
                         Type = PassengerType.AirlineEmployee,
                         Age = 21
                     },
                     new Passenger
                     {
                         Name = "Test3",
                         Type = PassengerType.AirlineEmployee,
                         Age = 22
                     },
                     new Passenger
                     {
                         Name = "Test4",
                         Type = PassengerType.AirlineEmployee,
                         Age = 23
                     },
                     new Passenger
                     {
                         Name = "Test5",
                         Type = PassengerType.AirlineEmployee,
                         Age = 24
                     },
                     new Passenger
                     {
                         Name = "Test6",
                         Type = PassengerType.AirlineEmployee,
                         Age = 26
                     },
                     new Passenger
                     {
                         Name = "Test7",
                         Type = PassengerType.AirlineEmployee,
                         Age = 27
                     },
                     new Passenger
                     {
                         Name = "Test8",
                         Type = PassengerType.AirlineEmployee,
                         Age = 28
                     },
                     new Passenger
                     {
                         Name = "Test9",
                         Type = PassengerType.General,
                         Age = 29
                     },
                     new Passenger
                     {
                         Name = "Test10",
                         Type = PassengerType.AirlineEmployee,
                         Age = 30
                     }
                 };

        private readonly List<Plane> _testPlanes = new List<Plane>
                {
                    new Plane { Id = 123, Name = "Antonov AN-2", NumberOfSeats = 12 },
                    new Plane { Id = 124, Name = "Bombardier Q400", NumberOfSeats = 78 },
                    new Plane { Id = 125, Name = "ATR 640", NumberOfSeats = 74 }
                };

        private readonly FlightRoute _testRoute = new FlightRoute
        {
            Origin = "London",
            Destination = "Paris",
            BaseCost = 50,
            BasePrice = 100,
            LoyaltyPointsGained = 5,
            MinimumTakeOffPercentage = 0.7
        };
        #endregion

        private ScheduledFlight _scheduledFlight;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            _scheduledFlight = new ScheduledFlight(_testRoute, _testPlanes[0]);

            foreach (var passenger in _testPassengers)
            {
                _scheduledFlight.AddPassenger(passenger);
            }
        }

        [Test]
        public void Constructor_FlightRouteAndPlaneSupplied_ScheduledFlightHasCorrectFlightRoute()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            Assert.AreEqual("London to Paris", sf.Route.Title);
        }


        [Test]
        public void Constructor_FlightRouteAndPlaneSupplied_ScheduledFlightHasCorrectPlane()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            Assert.AreEqual(_testPlanes[0].Id, sf.Aircraft.Id);
        }


        [Test]
        public void Constructor_FlightRouteAndPlaneSupplied_ScheduledFlightHasEmptyPassengerList()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);
            
            Assert.AreEqual(new List<Passenger>(), sf.Passengers);
        }

        [Test]
        public void AddPassenger_ValidPassengerAdded_AddsPassengerToScheduledFlight()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = PassengerType.General, Name = "Test", Age = 22 });

            Assert.AreEqual(1, sf.Passengers.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(7)]
        public void AddPassenger_ValidPassengerAdded_SeatsTakenIncreases(int count)
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            for (var i = 1; i <= count; i++)
            {
                sf.AddPassenger(new Passenger {Type = PassengerType.General, Name = "Test", Age = 22});
            }

            Assert.AreEqual(count, sf.SeatsTaken);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(7)]
        public void AddPassenger_GeneralPassengerAdded_CostOfFlightIncreases(int count)
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            for (var i = 1; i <= count; i++)
            {
                sf.AddPassenger(new Passenger { Type = PassengerType.General, Name = "Test", Age = 22 });
            }

            Assert.AreEqual(_testRoute.BaseCost*count, sf.CostOfFlight);
        }

        [TestCase(PassengerType.AirlineEmployee, 0)]
        [TestCase(PassengerType.Discounted, 50)]
        [TestCase(PassengerType.General, 100)]
        public void AddPassenger_NonLoyaltyPassengerAdded_PassengerPaysCorrectPrice(PassengerType type, double price)
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = type, Name = "Test", Age = 22 });

            Assert.AreEqual(price, sf.ProfitFromFlight);
        }

        [TestCase(PassengerType.AirlineEmployee, 1)]
        [TestCase(PassengerType.Discounted, 0)]
        [TestCase(PassengerType.General, 1)]
        [TestCase(PassengerType.LoyaltyMember, 2)]
        public void AddPassenger_PassengerAdded_PassengerGetsCorrectNumberOfBags(PassengerType type, int numBags)
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = type, Name = "Test", Age = 22 });

            Assert.AreEqual(numBags, sf.TotalExpectedBaggage);
        }

        [Test]
        public void AddPassenger_LoyaltyPassengerUsingPointsAdded_PassengerDoesNotPay()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = PassengerType.LoyaltyMember, Name = "Test", Age = 22, LoyaltyPoints = 2000, IsUsingLoyaltyPoints = true });

            Assert.AreEqual(0, sf.ProfitFromFlight);
        }

        [Test]
        public void AddPassenger_LoyaltyPassengerUsingPointsAdded_LoyaltyPointsRedeemedIncreases()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = PassengerType.LoyaltyMember, Name = "Test", Age = 22, LoyaltyPoints = 2000, IsUsingLoyaltyPoints = true });

            Assert.AreEqual(100, sf.TotalLoyaltyPointsRedeemed);
        }

        [Test]
        public void AddPassenger_LoyaltyPassengerUsingPointsAdded_PassengersLoyaltyPointsDecrease()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            var passenger = new Passenger
            {
                Type = PassengerType.LoyaltyMember,
                Name = "Test",
                Age = 22,
                LoyaltyPoints = 2000,
                IsUsingLoyaltyPoints = true
            };

            sf.AddPassenger(passenger);

            Assert.AreEqual(1900, passenger.LoyaltyPoints);
        }

        [Test]
        public void AddPassenger_LoyaltyPassengerNotUsingPointsAdded_PassengerPaysFullPrice()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = PassengerType.LoyaltyMember, Name = "Test", Age = 22, LoyaltyPoints = 2000, IsUsingLoyaltyPoints = false });

            Assert.AreEqual(100, sf.ProfitFromFlight);
        }

        [Test]
        public void AddPassenger_LoyaltyPassengerNotUsingPointsAdded_LoyaltyPointsAccruedIncreases()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(new Passenger { Type = PassengerType.LoyaltyMember, Name = "Test", Age = 22, LoyaltyPoints = 2000, IsUsingLoyaltyPoints = false });

            Assert.AreEqual(5, sf.TotalLoyaltyPointsAccrued);
        }

        [Test]
        public void GetSummary_ValidProceedingFlight_SummaryNameIsCorrect()
        {
            var summaryString = _scheduledFlight.GetSummary(RuleType.Normal, _testPlanes);

            Assert.AreEqual("Flight summary for London to Paris", summaryString.Substring(0, 34));
        }

        [TestCase("Total passengers: ")]
        [TestCase("General sales: ")]
        [TestCase("Loyalty member sales: ")]
        [TestCase("Airline employee comps: ")]
        [TestCase("Discounted sales: ")]
        [TestCase("Total expected baggage: ")]
        [TestCase("Total revenue from flight: ")]
        [TestCase("Total costs from flight: ")]
        [TestCase("Total loyalty points given away: ")]
        [TestCase("Total loyalty points redeemed: ")]
        [TestCase("Flight generating profit of: ")]
        [TestCase("THIS FLIGHT MAY PROCEED.")]
        public void GetSummary_ValidProceedingFlight_MentionsReleventInfo(string str)
        {
            var summaryString = _scheduledFlight.GetSummary(RuleType.Normal, _testPlanes);

            Assert.IsTrue(summaryString.Contains(str));
        }

        [Test]
        public void GetSummary_FlightGeneratingLoss_MentionsLoss()
        {
            var highCostFlight = new FlightRoute
            {
                Origin = _testRoute.Origin,
                Destination = _testRoute.Destination,
                BaseCost = 1000000,
                BasePrice = _testRoute.BasePrice,
                MinimumTakeOffPercentage = _testRoute.MinimumTakeOffPercentage,
                LoyaltyPointsGained = _testRoute.LoyaltyPointsGained
            };

            var sf = new ScheduledFlight(highCostFlight, _testPlanes[0]);

            foreach (var passenger in _testPassengers)
            {
                sf.AddPassenger(passenger);
            }

            var summaryString = sf.GetSummary(RuleType.Normal, _testPlanes);

            Assert.IsTrue(summaryString.Contains("Flight making loss of: "));
        }

        [Test]
        public void GetSummary_NonProceedingFlightWithNoViableAlternativeAircraft_OnlyMentionsFlightCannotProceed()
        {
            var sf = new ScheduledFlight(_testRoute, new Plane {Name = "Test", NumberOfSeats = 2, Id = 1});

            foreach (var passenger in _testPassengers)
            {
                sf.AddPassenger(passenger);
            }

            var summaryString = sf.GetSummary(RuleType.Normal, new List<Plane>());

            Assert.AreEqual("THIS FLIGHT MAY NOT PROCEED.", summaryString.Substring(summaryString.Length - 28));
        }

        [Test]
        public void GetSummary_NonProceedingFlightWithViableAlternativeAircraft_MentionsFlightCannotProceedAndPossibleAlternatives()
        {
            var sf = new ScheduledFlight(_testRoute, new Plane { Name = "Test", NumberOfSeats = 2, Id = 1 });

            foreach (var passenger in _testPassengers)
            {
                sf.AddPassenger(passenger);
            }

            var summaryString = sf.GetSummary(RuleType.Normal, _testPlanes);

            Assert.IsTrue(summaryString.Contains("THIS FLIGHT MAY NOT PROCEED."));
            Assert.IsTrue(summaryString.Contains(" could handle this flight."));
        }

        [Test]
        public void GetSummary_NormalRulesRevenueFromFlightDoesNotCoverCost_FlightMayNotProceed()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(_testPassengers[0]);

            var summaryString = sf.GetSummary(RuleType.Normal, _testPlanes);

            Assert.IsTrue(summaryString.Contains("THIS FLIGHT MAY NOT PROCEED."));
        }

        [Test]
        public void GetSummary_NormalRulesTooManyPassengersForAircraft_FlightMayNotProceed()
        {
            var sf = new ScheduledFlight(_testRoute, new Plane {Name = "Test", NumberOfSeats = 2, Id = 1});

            foreach (var passenger in _testPassengers)
            {
                sf.AddPassenger(passenger);
            }

            var summaryString = sf.GetSummary(RuleType.Normal, _testPlanes);

            Assert.IsTrue(summaryString.Contains("THIS FLIGHT MAY NOT PROCEED."));
        }

        [Test]
        public void GetSummary_NormalRulesFewerPassengersThanMinimumPercentage_FlightMayNotProceed()
        {
            //use a flight with no costs to make sure it doesn't fail because of no revenue.
            var noCostFlight = new FlightRoute
            {
                Origin = _testRoute.Origin,
                Destination = _testRoute.Destination,
                BaseCost = 0,
                BasePrice = _testRoute.BasePrice,
                MinimumTakeOffPercentage = _testRoute.MinimumTakeOffPercentage,
                LoyaltyPointsGained = _testRoute.LoyaltyPointsGained
            };

            var sf = new ScheduledFlight(noCostFlight, _testPlanes[0]);

            sf.AddPassenger(_testPassengers[0]);

            var summaryString = sf.GetSummary(RuleType.Normal, _testPlanes);

            Assert.IsTrue(summaryString.Contains("THIS FLIGHT MAY NOT PROCEED."));
        }

        [Test]
        public void GetSummary_RelaxedRulesRevenueFromFlightDoesNotCoverCostAirlineEmployeesDoNotExceedMinimumPassengerNumber_FlightMayNotProceed()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            sf.AddPassenger(_testPassengers[0]);

            var summaryString = sf.GetSummary(RuleType.Relaxed, _testPlanes);

            Assert.IsTrue(summaryString.Contains("THIS FLIGHT MAY NOT PROCEED."));
        }

        [Test]
        public void GetSummary_RelaxedRulesRevenueFromFlightDoesNotCoverCostAirlineEmployeesExceedMinimumPassengerNumber_FlightMayProceed()
        {
            var sf = new ScheduledFlight(_testRoute, _testPlanes[0]);

            foreach (var passenger in _employeePassengers)
            {
                sf.AddPassenger(passenger);
            }

            var summaryString = sf.GetSummary(RuleType.Relaxed, _testPlanes);

            Assert.IsTrue(summaryString.Contains("THIS FLIGHT MAY PROCEED."));
        }
    }
}