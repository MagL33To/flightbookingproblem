﻿namespace FlightBooking.Core.Classes.Rules
{
    public class GeneralPassengerRules : RuleBase
    {
        //general passenger pays full price
        public override double FractionOfPricePaid => 1;
        //general passenger is allowed 1 bag
        public override int BaggageAllowed => 1;
    }
}