﻿using FlightBooking.Core.Classes;
using FlightBooking.Core.Enums;
using NUnit.Framework;

namespace FlightBooking.Test
{
    [TestFixture]
    public class PassengerTests
    {
        [Test]
        public void General_DiscountedPasenger_CannotHaveBaggage()
        {
            var passenger = new Passenger
            {
                Type = PassengerType.Discounted,
                Name = "Test1",
                Age = 20
            };

            Assert.AreEqual(0, passenger.AllowedBags);
        }

        [TestCase(PassengerType.AirlineEmployee)]
        [TestCase(PassengerType.General)]
        [TestCase(PassengerType.LoyaltyMember)]
        public void General_NonDiscountedPasenger_CanHaveBaggage(PassengerType type)
        {
            var passenger = new Passenger
            {
                Type = type,
                Name = "Test1",
                Age = 20
            };

            Assert.Greater(passenger.AllowedBags, 0);
        }
        [Test]
        public void General_DiscountedPasenger_CannotAccrueLoyaltyPoints()
        {
            var passenger = new Passenger
            {
                Type = PassengerType.Discounted,
                Name = "Test1",
                Age = 20,
                LoyaltyPoints = 500000
            };

            Assert.AreEqual(0, passenger.LoyaltyPoints);
        }

        [TestCase(PassengerType.AirlineEmployee)]
        [TestCase(PassengerType.General)]
        [TestCase(PassengerType.LoyaltyMember)]
        public void General_NonDiscountedPasenger_CanAccrueLoyaltyPoints(PassengerType type)
        {
            var passenger = new Passenger
            {
                Type = type,
                Name = "Test1",
                Age = 20,
                LoyaltyPoints = 5000
            };

            Assert.AreEqual(5000, passenger.LoyaltyPoints);
        }

        [Test]
        public void Constructor_DiscountedPassenger_ReturnedPassengerIsAllowedNoBags()
        {
            var passenger = new Passenger(PassengerType.Discounted, new[] { "add", "discounted", "Stefan", "28" });

            Assert.AreEqual(0, passenger.AllowedBags);
        }

        [Test]
        public void Constructor_LoyaltyPassenger_ReturnedPassengerHasLoyaltyPointsSet()
        {
            const int loyaltyPoints = 1000;

            var passenger = new Passenger(PassengerType.LoyaltyMember, new[] { "add", "loyalty", "Stefan", "28", loyaltyPoints.ToString(), "false" });

            Assert.AreEqual(loyaltyPoints, passenger.LoyaltyPoints);
        }

        [Test]
        public void Constructor_LoyaltyPassenger_ReturnedPassengerHasLoyaltyUsageSet()
        {
            const bool loyaltyUsage = true;

            var passenger = new Passenger(PassengerType.LoyaltyMember, new[] { "add", "loyalty", "Stefan", "28", "1000", loyaltyUsage.ToString() });

            Assert.AreEqual(loyaltyUsage, passenger.IsUsingLoyaltyPoints);
        }

        [TestCase(PassengerType.General, new[] { "add", "general", "Steve", "30" })]
        [TestCase(PassengerType.AirlineEmployee, new[] { "add", "airline", "Steve", "30" })]
        [TestCase(PassengerType.LoyaltyMember, new[] { "add", "loyalty", "Steve", "30", "1000", "true" })]
        [TestCase(PassengerType.Discounted, new[] { "add", "discount", "Steve", "30" })]
        public void Constructor_ValidInput_ExpectedPassengerIsReturned(PassengerType type, string[] passengerSegments)
        {
            var passenger = new Passenger(type, passengerSegments);

            Assert.AreEqual(type, passenger.Type);
            Assert.AreEqual(passengerSegments[2], passenger.Name);
            Assert.AreEqual(passengerSegments[3], passenger.Age.ToString());
        }
    }
}